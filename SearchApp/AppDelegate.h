//
//  AppDelegate.h
//  SearchApp
//
//  Created by Alexander López Guerrero on 24/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

