//
//  MainViewController.h
//  SearchApp
//
//  Created by Alexander López Guerrero on 24/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DataSearch;

@interface MainViewController : UIViewController

@property (strong, nonatomic) DataSearch *dataArray;

@end
