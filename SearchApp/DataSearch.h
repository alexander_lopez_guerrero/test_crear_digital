//
//  DataSearch.h
//  SearchApp
//
//  Created by Alexander López Guerrero on 24/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataSearch : NSObject

@property (strong,nonatomic) NSArray *clothesArray;
@property (strong,nonatomic) NSArray *brandsArray;

-(NSArray *)getWebSearch:(NSString *)searchText :(NSString *)urlClothes :(NSString *) urlBrands;

@end
