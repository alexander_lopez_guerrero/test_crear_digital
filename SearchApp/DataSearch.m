//
//  DataSearch.m
//  SearchApp
//
//  Created by Alexander López Guerrero on 24/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "DataSearch.h"

@implementation DataSearch

-(NSArray *)getWebSearch:(NSString *)searchText :(NSString *)urlClothes :(NSString *) urlBrands;
{
    
    /* Clothes Data */
    NSURL *clothesURL = [NSURL URLWithString:urlClothes];
    //NSData *clothesJsonData = [NSData dataWithContentsOfURL:clothesURL];
    NSData *clothesJsonData = [NSData dataWithContentsOfURL:clothesURL];
    
    NSError *clothesError = nil;
    
    NSDictionary *clothesData = [NSJSONSerialization JSONObjectWithData:clothesJsonData options:0 error:&clothesError];
    
    self.clothesArray = [clothesData valueForKeyPath:@"clothes.name"];
    
    /* Brands Data */
    NSURL *brandsURL = [NSURL URLWithString:urlBrands];
    NSData *brandsJsonData = [NSData dataWithContentsOfURL:brandsURL];
    NSError *brandsError = nil;
    
    NSDictionary *brandsData = [NSJSONSerialization JSONObjectWithData:brandsJsonData options:0 error:&brandsError];
    
    self.brandsArray = [brandsData valueForKeyPath:@"brands.name"];
    
    return self.clothesArray;
    
}

@end
