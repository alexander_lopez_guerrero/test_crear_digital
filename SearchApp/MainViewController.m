//
//  MainViewController.m
//  SearchApp
//
//  Created by Alexander López Guerrero on 24/05/16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "MainViewController.h"
#import "DataSearch.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *urlClothes = @"http://private-8a477-pruebatl.apiary-mock.com/clothes";
    NSString *urlBrands = @"http://private-8a477-pruebatl.apiary-mock.com/brands";
    
    self.dataArray = [[DataSearch alloc] init];
    
    NSArray *result = [self.dataArray getWebSearch:@"Gap Floral Pants" :@"http://private-8a477-pruebatl.apiary-mock.com/clothes" :@"http://private-8a477-pruebatl.apiary-mock.com/brands"];
    
    NSLog(@"Result is: %@", result);
    
    //self.dataArray.getWebSearch:@"Gap Floral Pants" :urlClothes : urlBrands;
    
    //DataSearch *dataArray = [[DataSearch alloc] init];
    
    //dataArray.getWebSearch
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
